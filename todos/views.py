
from django import template
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView, DetailView, CreateView, UpdateView
# from django.views.generic import DetailView, CreateView, UpdateView
# from django.views.generic.edit import CreateItemView, DeleteItemView, UpdateItemView
from django.shortcuts import render

from todos.models import TodoList

# Create your views here.
class TodoListView(ListView):
    model= TodoList
    template_name = "todos/list.html"

class TodoDetailView(DetailView):
    model= TodoList
    template_name = "todos/detail.html"

class TodoCreateView(CreateView):
    model= TodoList
    template_name = "todos/new.html"
    fields = ["name", "description"]

    def get_succes_url(self):
        return reverse_lazy("detail_todos")

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"

class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"

class TodoItemCreateView(CreateView):
    model= TodoList
    template_name = "todos/newitem.html"
    fields = ["name", "description"]

class TodoItemUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edititem.html"

class TodoItemDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/deleteitem.html"