from django.urls import path
from todos.views import TodoCreateView, TodoDeleteView, TodoListView, TodoUpdateView, TodoDetailView
from todos.views import TodoItemCreateView, TodoItemDeleteView, TodoItemUpdateView



urlpatterns = [
    
    path("", TodoListView.as_view(), name = "todos_list"),
    path('todos/<int:pk>detail/', TodoDetailView.as_view(), name= "todos_detail"),
    path("todos/create/", TodoCreateView.as_view(), name= "todos_create"),
    path("todos/<int:pk>edit/", TodoUpdateView.as_view(), name= "todos_update"),
    path("todo/<int:pk>/delete/", TodoDeleteView.as_view(), name = "todos_deleter"),
    path("todos/itemcreate/", TodoItemCreateView.as_view(), name= "todositem_create"),
    path("todo/<int:pk>/itemdelete/", TodoItemDeleteView.as_view(), name = "todositem_deleter"),
    path("todos/<int:pk>itemedit/", TodoItemUpdateView.as_view(), name= "todositem_update"),
]